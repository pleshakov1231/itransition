## Run Docker

## Configure

- **Add and edit .env**
```
cp .env-example .env
```


## Useful commands:


- **Build containers:**
```
USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose up -d --build
```

- **Stop and remove containers:**
```
docker-compose down --remove-orphans
```

