

## Useful commands:

- **Enter to container:**
```
docker exec -it itr-task bash
```

- **Go to directory:**
```
cd itr-task.loc
```

- **Install composer:**
```
composer install
```

- **Run migrations:**
```
bin/console doctrine:migrations:migrate
```

- **Add data to DB:**
```
bin/console import-products
```
