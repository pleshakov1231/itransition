<?php declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as AcmeAssert;

class ProductDTO
{

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    private $strproductname;

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    private $strproductdesc;

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    private $strproductcode;

    /**
     * @var float
     *
     * @Assert\NotNull
     * @Assert\Positive
     * @Assert\LessThan(1000,
     *     message="Cost should be less than 1000")
     * @AcmeAssert\Cost()
     */
    private $cost;

    /**
     * @var int
     *
     * @Assert\NotBlank
     */
    private $stock;


    private $discontinued;


    public function __construct(string $strproductname, string $strproductdesc, string $strproductcode, $cost, $stock, $discontinued)
    {
        $this->strproductname = $strproductname;
        $this->strproductdesc = $strproductdesc;
        $this->strproductcode = $strproductcode;
        $this->cost = $cost;
        $this->stock = $stock;
        $this->discontinued = $discontinued;
    }

    /**
     * @return string
     */
    public function getStrproductname(): string
    {
        return $this->strproductname;
    }

    /**
     * @return string
     */
    public function getStrproductdesc(): string
    {
        return $this->strproductdesc;
    }

    /**
     * @return string
     */
    public function getStrproductcode(): string
    {
        return $this->strproductcode;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @return mixed
     */
    public function getDiscontinued()
    {
        return $this->discontinued;
    }

}
