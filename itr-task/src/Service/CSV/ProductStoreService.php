<?php declare(strict_types=1);

namespace App\Service\CSV;

use App\Entity\Tblproductdata;
use App\Repository\TblproductdataRepository;
use DateTime;

class ProductStoreService
{
    private TblproductdataRepository $repository;

    public function __construct(TblproductdataRepository $repository)
    {
        $this->repository = $repository;
    }

    public function storeProduct($csvRow): Tblproductdata
    {
        $date = new DateTime();
        $product = new Tblproductdata();

        $product->setStrproductname($csvRow["Product Name"]);
        $product->setStrproductdesc($csvRow["Product Description"]);
        $product->setStrproductcode($csvRow["Product Code"]);
        $product->setDtmadded($date);
        $product->setStmtimestamp($date);
        if($csvRow["Discontinued"] === "yes"){
            $product->setDtmdiscontinued($date);
        }
        $product->setStock((int)$csvRow["Stock"]);
        $product->setCost((float)$csvRow["Cost in GBP"]);

        $this->repository->store($product);

        return $product;
    }
}
