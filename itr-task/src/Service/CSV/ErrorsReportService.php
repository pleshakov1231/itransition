<?php declare(strict_types=1);

namespace App\Service\CSV;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\ConstraintViolationList;

class ErrorsReportService
{

    public function createErrorReport(array $errorsForReport, InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        $io->note('Validate errors of CSV Data');
        $ioTableData = [];

        foreach ($errorsForReport as $errorArray){
            if(!count($errorArray) !== 0){
                $ioTableData = $this->fillReportRow($errorArray, $ioTableData);
            }
        }

        $io->table(
            ['Product Code', 'Product Name', 'Product Description', 'Cost in GBP', 'Stock', 'Discontinued', 'Property', 'Error'],
            $ioTableData
        );
    }

    private function fillReportRow(ConstraintViolationList $errorArray, array $ioTableData): array
    {
        $ioTableRow = [];

        foreach ($errorArray as $violation){
            $dto = $violation->getRoot();
            array_push($ioTableRow,
                $dto->getStrproductcode(),
                $dto->getStrproductname(),
                $dto->getStrproductdesc(),
                $dto->getCost(),
                $dto->getStock(),
                $dto->getDiscontinued(),
                $violation->getPropertyPath(),
                $violation->getMessage(),
            );
            $ioTableData[] = $ioTableRow;
        }

        return $ioTableData;
    }
}
