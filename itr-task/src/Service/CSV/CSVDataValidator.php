<?php declare(strict_types=1);

namespace App\Service\CSV;

use App\DTO\ProductDTO;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CSVDataValidator
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validateRow(array $csvRow)
    {
        $dto = new ProductDTO(
            $csvRow["Product Name"],
            $csvRow["Product Description"],
            $csvRow["Product Code"],
            (float)$csvRow["Cost in GBP"],
            (int)$csvRow["Stock"],
            $csvRow["Discontinued"]
        );

        return $this->validator->validate($dto);


    }

}
