<?php declare(strict_types=1);

namespace App\Service\CSV;

use League\Csv\Modifier\MapIterator;
use League\Csv\Reader;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\HttpKernel\KernelInterface;

class CSVProvider
{

    private KernelInterface $appKernel;

    public function __construct(KernelInterface $appKernel)
    {
        $this->appKernel = $appKernel;
    }

    public function parseCSV(string $filePath): array
    {
        try {
            if (!file_exists($this->appKernel->getProjectDir() . $filePath))
            {
                throw new FileNotFoundException("CSV file not found in App/data directory");
            }
        } catch (FileNotFoundException $e) {
            echo $e->getMessage();
            die;
        }

        $reader = Reader::createFromPath($this->appKernel->getProjectDir() . $filePath);

        $dataArray = $this->normalizeArray($reader->fetchAssoc());

        return $dataArray;
    }

    private function normalizeArray(MapIterator $csvData): array
    {
        $csvDataArray = $this->convertToAssocArray($csvData);
        $this->checkForDublicates($csvDataArray);

        return $csvDataArray;
    }

    private function convertToAssocArray(MapIterator $mapIteratorCollection): array
    {
        $resultsToArray = [];
        foreach ($mapIteratorCollection as $row){
            $resultsToArray[] = $row;
        }

        return $resultsToArray;
    }

    private function checkForDublicates(array $csvDataArray): void
    {
        $codes = [];
        foreach ($csvDataArray as $row){
            if(in_array($row["Product Code"], $codes)){
                throw new \LogicException('The CSV file contains duplicate product codes. Please, remove duplicates from file');
            }
        }
    }
}
