<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "ANNOTATION"})
 */
class Cost extends Constraint
{
    public $message = 'If the cost is less than 5, stock must be more than 10';
}
