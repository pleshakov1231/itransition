<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CostValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if($value <= 5 && $this->context->getRoot()->getStock() < 10){
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
