<?php declare(strict_types=1);

namespace App\Command;

use App\Service\CSV\CSVDataValidator;
use App\Service\CSV\CSVNormalizer;
use App\Service\CSV\CSVProvider;
use App\Service\CSV\ErrorsReportService;
use App\Service\CSV\ProductStoreService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StoreDataCommand extends Command
{
    protected static $defaultName = 'import-products';

    private const CSV_PATH = "/data/stock.csv";

    /**
     * @var CSVDataValidator
     */
    private CSVDataValidator $validator;

    /**
     * @var CSVProvider
     */
    private CSVProvider $csvProvider;

    /**
     * @var ProductStoreService
     */
    private ProductStoreService $storeService;

    /**
     * @var ErrorsReportService
     */
    private ErrorsReportService $errorsReportService;

    public function __construct(CSVProvider $csvProvider,
                                CSVDataValidator $validator,
                                ProductStoreService $storeService,
                                ErrorsReportService $errorsReportService
                                )
    {
        parent::__construct();
        $this->validator = $validator;
        $this->csvProvider = $csvProvider;
        $this->storeService = $storeService;
        $this->errorsReportService = $errorsReportService;
    }

    protected function configure(): void
    {
        $this
            ->setName('Import products from CSV')
            ->setDescription('Parse CSV file and store data to DB')
            ->setDefinition(
                new InputDefinition(array(
                    new InputOption('test', 't'),
                ))
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//      -------
//      Get data from CSV file
//      -------
        $csvData = $this->csvProvider->parseCSV(self::CSV_PATH);

        $errorsForReport = [];
        foreach ($csvData as $row){
//          -------
//          Validate row with product data. Checking data for compliance with conditions.
//          -------
            $errors = $this->validator->validateRow($row);
            $errorsForReport[] = $errors;

//          -------
//          Saving the product to the database if there are no errors
//          -------
            if($errors->count() === 0 && !$input->getOption("test")){
                $this->storeService->storeProduct($row);
            }
        }

//      -------
//      Create report for console output
//      -------
        $this->errorsReportService->createErrorReport($errorsForReport, $input, $output);

        return Command::SUCCESS;
    }


}


