<?php declare(strict_types=1);

namespace App\Tests\Unit\Command;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class StoreDataCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('import-products');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--test' => 'test',
        ]);

        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('P0011', $output);
        $this->assertStringContainsString('P0015', $output);
        $this->assertStringContainsString('P0017', $output);
        $this->assertStringContainsString('P0027', $output);
        $this->assertStringContainsString('P0028', $output);
    }
}
