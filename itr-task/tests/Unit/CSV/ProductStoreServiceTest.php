<?php declare(strict_types=1);

namespace App\Tests\Unit\CSV;

use App\Entity\Tblproductdata;
use App\Repository\TblproductdataRepository;
use App\Service\CSV\ProductStoreService;
use PHPUnit\Framework\TestCase;
use DateTime;

class ProductStoreServiceTest extends TestCase
{
    private TblproductdataRepository $repository;

    private ProductStoreService $storeService;

    protected function setUp(): void
    {
        $this->repository = $this->createMock(TblproductdataRepository::class);
        $this->storeService = new ProductStoreService($this->repository);
    }


    public function testStoreProduct()
    {
        $csvRow = $this->dataArray();

        $date = new DateTime();
        $product = new Tblproductdata();

        $product->setStrproductname($csvRow["Product Name"]);
        $product->setStrproductdesc($csvRow["Product Description"]);
        $product->setStrproductcode($csvRow["Product Code"]);
        $product->setDtmadded($date);
        $product->setStmtimestamp($date);
        if($csvRow["Discontinued"] === "yes"){
            $product->setDtmdiscontinued($date);
        }
        $product->setStock((int)$csvRow["Stock"]);
        $product->setCost((float)$csvRow["Cost in GBP"]);

        $st = $this->storeService->storeProduct($csvRow);

        self::assertEquals($product->getStrproductcode(), $st->getStrproductcode());
        self::assertEquals($product->getDtmdiscontinued(), $st->getDtmdiscontinued());
        self::assertEquals($product->getStrproductname(), $st->getStrproductname());
        self::assertEquals($product->getStrproductdesc(), $st->getStrproductdesc());
        self::assertEquals($product->getCost(), $st->getCost());
        self::assertEquals($product->getStock(), $st->getStock());
    }

    public function dataArray(): array
    {
        return [
            "Product Code" => "P0001",
            "Product Name" => "TV",
            "Product Description" => "32” Tv",
            "Stock" => "10",
            "Cost in GBP" => "399.99",
            "Discontinued" => "",
        ];
    }
}
