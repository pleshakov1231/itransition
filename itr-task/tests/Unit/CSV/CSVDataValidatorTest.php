<?php declare(strict_types=1);

namespace App\Tests\Unit\CSV;

use App\DTO\ProductDTO;
use App\Service\CSV\CSVDataValidator;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class CSVDataValidatorTest extends WebTestCase
{

    private CSVDataValidator $csvValidator;

    protected ?KernelBrowser $client = null;

    protected function setUp(): void
    {
        parent::ensureKernelShutdown();

        $this->client = static::createClient();
        $this->csvValidator = $this->client->getContainer()->get("test.App\Service\CSV\CSVDataValidator");
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @dataProvider providerForSuccessProductData
     *
     * @param array $csvRow
     */
    public function testSuccessValidate(array $csvRow)
    {
        $results = $this->csvValidator->validateRow($csvRow);

        $this->assertEquals(0, $results->count());

    }

    /**
     * @dataProvider providerForErrorProductData
     *
     * @param array $csvRow
     */
    public function testErrorValidate(array $csvRow)
    {
        $results = $this->csvValidator->validateRow($csvRow);
        $this->assertGreaterThan(0, $results->count());
        $this->assertEquals($csvRow["message"], $results->get(0)->getMessage());
    }

    /**
     * @return array
     */
    public function providerForSuccessProductData(): array
    {
        return [
            [
                [
                    "Product Code" => "P0001",
                    "Product Name" => "TV",
                    "Product Description" => "32” Tv",
                    "Stock" => "10",
                    "Cost in GBP" => "5",
                    "Discontinued" => "",
                ],
                [
                    "Product Code" => "P0001",
                    "Product Name" => "TV",
                    "Product Description" => "32” Tv",
                    "Stock" => "10",
                    "Cost in GBP" => "4",
                    "Discontinued" => "",
                ],
                [
                    "Product Code" => "P0001",
                    "Product Name" => "TV",
                    "Product Description" => "32” Tv",
                    "Stock" => "9",
                    "Cost in GBP" => "5",
                    "Discontinued" => "",
                ],
            ]
        ];
    }

    /**
     * @return array
     */
    public function providerForErrorProductData(): array
    {
        return [
            [
                [
                    "Product Code" => "P0001",
                    "Product Name" => "TV",
                    "Product Description" => "32” Tv",
                    "Stock" => "9",
                    "Cost in GBP" => "4",
                    "Discontinued" => "",
                    "message" => "If the cost is less than 5, stock must be more than 10"
                ],
                [
                    "Product Code" => "P0001",
                    "Product Name" => "TV",
                    "Product Description" => "32” Tv",
                    "Stock" => "9",
                    "Cost in GBP" => "0",
                    "Discontinued" => "",
                    "message" => "This value should be positive"
                ],
                [
                    "Product Code" => "P0002",
                    "Product Name" => "XBOX360",
                    "Product Description" => "Best.console.ever",
                    "Stock" => "6",
                    "Cost in GBP" => "2000",
                    "Discontinued" => "",
                    "message" => "Cost should be less than 1000",
                ],
                [
                    "Product Code" => "P0002",
                    "Product Name" => "",
                    "Product Description" => "",
                    "Stock" => "6",
                    "Cost in GBP" => "75.75",
                    "Discontinued" => "yes",
                    "message" => "This value should not be blank.",
                ],
                [
                    "Product Code" => "P0002",
                    "Product Name" => "XBOX360",
                    "Product Description" => "",
                    "Stock" => "6",
                    "Cost in GBP" => "56",
                    "Discontinued" => "",
                    "message" => "This value should not be blank.",
                ],
            ]
        ];
    }
}
