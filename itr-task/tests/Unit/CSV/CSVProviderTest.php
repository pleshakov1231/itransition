<?php declare(strict_types=1);

namespace App\Tests\Unit\CSV;

use App\Service\CSV\CSVProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\KernelInterface;

class CSVProviderTest extends TestCase
{

    private CSVProvider $provider;

    private KernelInterface $kernel;

    protected function setUp(): void
    {
        $this->kernel = $this->createMock(KernelInterface::class);
        $this->provider = new CSVProvider($this->kernel);

    }

    public function testParseCSV()
    {

        $this->kernel->expects($this->atLeastOnce())
            ->method("getProjectDir")
            ->willReturn('/var/www/itr-task.loc/tests/Unit/CSV/fixtures/');

        $results = $this->provider->parseCSV("test_stock.csv");

        self::assertEquals($this->dataArray(), $results);
    }

    public function dataArray(): array
    {
    return [
            [
                "Product Code" => "P0001",
                "Product Name" => "TV",
                "Product Description" => "32” Tv",
                "Stock" => "10",
                "Cost in GBP" => "399.99",
                "Discontinued" => "",
            ],
            [
                "Product Code" => "P0002",
                "Product Name" => "Cd Player",
                "Product Description" => "Nice CD player",
                "Stock" => "11",
                "Cost in GBP" => "50.12",
                "Discontinued" => "yes",
            ],
            [
                "Product Code" => "P0003",
                "Product Name" => "VCR",
                "Product Description" => "Top notch VCR",
                "Stock" => "12",
                "Cost in GBP" => "39.33",
                "Discontinued" => "yes",
            ]
        ];
    }
}
